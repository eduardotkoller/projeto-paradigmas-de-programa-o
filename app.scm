(load "estoque.scm")
(load "channel.scm")
(import channel)
(use srfi-18)

(define (request-make msg resp)
  (define (get-msg)
    msg)
  (define (get-resp)
    resp)
  (lambda (m)
    (cond ((eq? m 'get-msg) get-msg)
					  ((eq? m 'get-resp) get-resp))))

(define (server-make)
  (define request-channel (make-bounded-channel 100))
  (define estoque (estoque-load))
  (define (get-req)
    request-channel)
  (define (serve)
    (let while ()
        (define request ((request-channel 'receive)))
        (if (not (null? request))
          (begin
            (define new-thread (make-thread (lambda ()
              (define msg ((request 'get-msg)))
              (define response-channel ((request 'get-resp)))
              (if (pair? msg)
                (cond ((eq? (car msg) 'lista) ; msg deve ser uma lista ('metodo arg1 arg2 arg3...)
                        ((response-channel 'send) ((estoque 'get-produtos))))
                      ((eq? (car msg) 'insere-produto) ; ('insere-produto nome quantidade)
                      	(if (symbol? (cadr msg))
                        	((response-channel 'send) ((estoque 'insere) (symbol->string (cadr msg)) (caddr msg)))
                        	((response-channel 'send) ((estoque 'insere) (cadr msg) (caddr msg)))))
                      ((eq? (car msg) 'insere) ; ('insere id quantidade)
                        ((response-channel 'send) ((((estoque 'get-produto) (cadr msg)) 'insere) (caddr msg))))
                      ((eq? (car msg) 'remove) ; ('remove id quantidade)
                        ((response-channel 'send) ((((estoque 'get-produto) (cadr msg)) 'remove) (caddr msg))))
                      ((eq? (car msg) 'help) ; ('help)
                        ((response-channel 'send) (list '(lista) '(insere-produto nome quantidade) '(insere id quantidade) '(remove id quantidade))))
                      (else ((response-channel 'send) 'unknown-method)))
              ((response-channel 'send) 'unknown-method)))))
             (thread-start! new-thread)))
      (while)))
   (lambda (m)
    (cond ((eq? m 'get-req) get-req)
					  ((eq? m 'serve) serve)
					  (else (lambda () 'unknown-method)))))
					  
					  
;exemplos de funcionamento do app com simulacao de clientes via threads

(define server (server-make))
(define request-channel ((server 'get-req)))

(define server-thread (make-thread (lambda () ((server 'serve)))))

(define client-thread (make-thread (lambda ()
                                      (define response-channel (make-sync-channel))
                                      (print "Interface de cliente inicializada. Para lista de métodos suportados, envie (help).")
                                      (let while ()
                                        (print "Entre com uma mensagem para o servidor:")
                                        (define req (request-make (read) response-channel))
                                        ((request-channel 'send) req)
                                        (print "Resposta do servidor:")
                                        (print ((response-channel 'receive)))
                                        (newline)
                                        (while)))))
                           
(define thread-list (list server-thread client-thread))

(newline)

(map thread-start! thread-list)
(map thread-join! thread-list)
                                      
                                      
                                      
                                      
                                      
                                      


