(define produto-make
	(lambda (id nome estoque)
		(define commit
			(lambda ()
				(define f (open-output-file (string-append "db/" (string-append (number->string id) ".prd"))))
				(write-line (number->string id) f)
				(write-line nome f)
				(write-line (number->string estoque) f)
				(close-output-port f)))
		(define insere!
			(lambda (quantidade)
				(set! estoque (+ estoque quantidade))
				(commit)
				estoque))
		(define remove!
			(lambda (quantidade)
				(set! estoque (- estoque quantidade))
				(commit)
				estoque))
		(define get-info
			(lambda ()
				(list id nome estoque)))
		(lambda (m) ;dispatcher
			(cond ((eq? m 'insere) insere!)
				  ((eq? m 'remove) remove!)
				  ((eq? m 'get-info) get-info)))))
