; channel definitions; all channels here are implemented as monitors, having
; all exposed methods mutual exclusion

(module channel (make-channel make-sync-channel make-bounded-channel)
  (import chicken scheme)
  (use srfi-18)
  
  ; a factory of synchronizers
  ; a synchronizer is an operation that wraps a procedure as a critical
  ; section; all procedures wrapped by the synchronizer will use the same
  ; lock, consequently, all  those procedures will be mutually exclusive between
  ; each other
  (define (make-synchronizer mx)
    (lambda (f)
      (define (synchronized-f . args)
        (mutex-lock! mx)
        (let ((result (apply f args)))
          (mutex-unlock! mx)
          result))
      synchronized-f))
  
  ; a bounded non-persistent queue
  (define (make-bounded-queue capacity)
    (define front '())
    (define rear '())
    (define occupation 0)
    (define (make-node v)
      (cons v '()))
    (define (enqueue v)
      (cond ((null? front) 
             (set! front (make-node v))
             (set! rear front)
             (set! occupation (+ occupation 1))
             #t)
            ((< occupation capacity)
             (let ((new-node (make-node v)))
               (set-cdr! rear new-node)
               (set! rear new-node)
               (set! occupation (+ occupation 1)))
             #t)
            (else #f)))
    (define (dequeue)
      (if (null? front)
          #f
          (begin
            (set! front (cdr front))
            (set! occupation (- occupation 1))
            #t)))
    (define (get-front)
      (cond ((null? front) front)
            (else (car front))))
    (define (get-rear)
      (cond ((null? rear) rear)
            (else (car rear))))
    (define (get-occupation)
      occupation)
    (define (full?)
      (= occupation capacity))
    (define (empty?)
      (= occupation 0))
    (lambda (m)
      (cond ((eq? m 'enqueue) enqueue)
            ((eq? m 'dequeue) dequeue)
            ((eq? m 'get-front) get-front)
            ((eq? m 'get-rear) get-rear)
            ((eq? m 'get-occupation) get-occupation)
            ((eq? m 'full?) full?)
            ((eq? m 'empty?) empty?))))
  
  ;; an unbounded non-persistent queue
  (define (make-unbounded-queue)
    (define front '())
    (define rear '())
    (define (make-node v)
      (cons v '()))
    (define (enqueue v)
      (cond ((null? front) 
             (set! front (make-node v))
             (set! rear front))
            (else
             (let ((new-node (make-node v)))
               (set-cdr! rear new-node)
               (set! rear new-node))))
      #t)
    (define (dequeue)
      (cond ((null? front) #f)
            (else (set! front (cdr front))
                  #t)))
    (define (get-front)
      (cond ((null? front) front)
            (else (car front))))
    (define (get-rear)
      (cond ((null? rear) rear)
            (else (car rear))))
    (define (empty?)
      (null? front))
    (lambda (m)
      (cond ((eq? m 'enqueue) enqueue)
            ((eq? m 'dequeue) dequeue)
            ((eq? m 'get-front) get-front)
            ((eq? m 'get-rear) get-rear)
            ((eq? m 'empty?) empty?)
            (else (error "unknown method")))))
  
  ; a channel with capacity for one message
  (define (make-sync-channel)
    (define buffer '())
    (define count 0)
    (define mx (make-mutex))
    (define synchronizer (make-synchronizer mx))
    (define cv-empty (make-condition-variable))
    (define cv-fill (make-condition-variable))
    
    (define (send message)
      (let while ()
        (if (full?)
          (begin
            (wait cv-fill)
            (while))))
      (set! count 1)
      (set! buffer message)
      (notify cv-empty))
    
    (define (receive)
      (let while ()
        (if (empty?)
            (begin
              (wait cv-empty)
              (while))))
      (set! count 0)
      (notify cv-fill)
      buffer)
    
    (define (empty?)
      (= count 0))
    
    (define (full?)
      (= count 1))
    
    (define (wait cv)
      (mutex-unlock! mx cv))
    
    (define (notify cv)
      (condition-variable-signal! cv))
    
    (define (notify-all cv)
      (condition-variable-broadcast! cv))
    
    (lambda (m)
      (cond ((eq? m 'send) (synchronizer send))
            ((eq? m 'receive) (synchronizer receive))
            ((eq? m 'empty?) (synchronizer empty?))
            ((eq? m 'full?) (synchronizer full?))
            (else (error "unknown method")))))
  
  ; a bounded (async) channel; when its buffer is full, it blocks the caller, 
  ; making message passing synchronous
  (define (make-bounded-channel capacity)
    (define buffer (make-bounded-queue capacity))
    (define mx (make-mutex))
    (define synchronizer (make-synchronizer mx))
    (define cv-fill (make-condition-variable))
    (define cv-has-message (make-condition-variable))
    
    (define (send message)
      (let while ()
        (if (full?)
          (begin
            (wait cv-fill)
            (while))))
      ((buffer 'enqueue) message)
      (notify cv-has-message))
      
    (define (receive)
      (let while ()
        (if (empty?)
            (begin
              (wait cv-has-message)
              (while))))
      (define message ((buffer 'get-front)))
      ((buffer 'dequeue))
      message)
    
    (define (empty?)
      ((buffer 'empty?)))
    
    (define (full?)
      ((buffer 'full?)))
    
    (define (wait cv)
      (mutex-unlock! mx cv))
    
    (define (notify cv)
      (condition-variable-signal! cv))
    
    (define (notify-all cv)
      (condition-variable-broadcast! cv))
    
    (lambda (m)
      (cond ((eq? m 'send) (synchronizer send))
            ((eq? m 'receive) (synchronizer receive))
            ((eq? m 'empty?) (synchronizer empty?))
            (else (error "unknown method")))))

  ; the channel has an unbounded queue (async), which is not 
  ; recommended in production environments
  (define (make-channel)
    (define buffer (make-unbounded-queue))
    (define mx (make-mutex))
    (define synchronizer (make-synchronizer mx))
    (define cv-has-message (make-condition-variable))
    
    (define (send message)
      ((buffer 'enqueue) message)
      (notify cv-has-message))
      
    (define (receive)
      (let while ()
        (if ((buffer 'empty?))
            (begin
              (wait cv-has-message)
              (while))))
      (define message ((buffer 'get-front)))
      ((buffer 'dequeue))
      message)
    
    (define (empty?)
      ((buffer 'empty?)))
    
    (define (wait cv)
      (mutex-unlock! mx cv))
    
    (define (notify cv)
      (condition-variable-signal! cv))
    
    (define (notify-all cv)
      (condition-variable-broadcast! cv))
    
    (lambda (m)
      (cond ((eq? m 'send) (synchronizer send))
            ((eq? m 'receive) (synchronizer receive))
            ((eq? m 'empty?) (synchronizer empty?))
            (else (error "unknown method"))))) )

